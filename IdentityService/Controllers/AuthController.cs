using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using IdentityService.Models;

namespace IdentityService.Controllers;


[ApiController]
[Route("[controller]")]
public class AuthController : ControllerBase
{
    private readonly X509Certificate2 _certificate;
	private readonly string _aud;
	private readonly string _iss;
	
    public AuthController(IConfiguration configuration)
    {
        var certificatePath = configuration["CertificatePath"];
        var certificatePassword = configuration["CertificatePassword"];
		_aud = configuration["Jwt:Audience"];
		_iss = configuration["Jwt:Issuer"];
        _certificate = new X509Certificate2(certificatePath, certificatePassword);
    }

    [HttpPost("login")]
    public IActionResult Login([FromBody] LoginRequest request)
    {
        // Replace this with your actual authentication logic (e.g., checking username and password in a database)
        if (request.Username == "sampleUser" && request.Password == "samplePassword")
        {
            var token = GenerateJwtToken(request.Username);
            return Ok(new { token });
        }

        return Unauthorized(new { message = "Invalid username or password" });
    }

    private string GenerateJwtToken(string username)
    {
        var rsaPrivateKey = _certificate.GetRSAPrivateKey();

        var signingCredentials = new SigningCredentials(new RsaSecurityKey(rsaPrivateKey), SecurityAlgorithms.RsaSha256);

        var tokenHandler = new JwtSecurityTokenHandler();
        var claims = new ClaimsIdentity(new[]
        {
            new Claim(ClaimTypes.Name, username),
			new Claim(JwtRegisteredClaimNames.Aud, _aud),
			new Claim(JwtRegisteredClaimNames.Iss, _iss)

        });

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = claims,
            Expires = DateTime.UtcNow.AddHours(1), // Token expiration time
            SigningCredentials = signingCredentials
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
}


