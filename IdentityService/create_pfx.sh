#!/bin/bash
#This script will create an RSA key pair and an X.509 certificate in PFX format using OpenSSL on Linux

# Generate a Private Key and a Certificate Signing Request (CSR)
# The following command will generate a private key and a CSR. The CSR will be used to generate the certificate:

openssl req -newkey rsa:2048 -keyout private-key.key -out certificate.csr

# Sign the CSR with a Certificate Authority (CA) or Self-Sign It
# We will self-sign the certificate for testing purposes
# If you have a CA that you want to sign your CSR, send the `certificate.csr` to the CA, and they will return a signed certificate.

openssl x509 -req -days 365 -in certificate.csr -signkey private-key.key -out certificate.crt
 
 
# Create a PKCS#12 (PFX) File
# The following command will combine the private key and the signed certificate into a PKCS#12 file (PFX):

openssl pkcs12 -export -out certificate.pfx -inkey private-key.key -in certificate.crt

# The following command will generate the public key for the certificate, and it should be copied to whatever service is validating data signed by the private key:

openssl x509 -pubkey -in certificate.crt -out public-key.pem

echo "You should have a certificate.pfx file that contains the private key and the signed certificate in PFX format. Remember to handle the PFX file and its password securely, especially in production environments."

echo "You should also have the corresponding public key for validation in public-key.pem"
