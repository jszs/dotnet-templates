1. create an X509 PFX certificate by running the create_pfx.sh script
2. copy the certificate.pfx file to /var/tmp/democertificate.pfx
3. on the service that will verify JWTs signed by the RSA private key, copy the public-key.pem file to /var/tmp/public-key.pem
4. export CertificatePassword=ReplaceWithYourPassword
5. dotnet run
