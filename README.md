# Dotnet Templates



## About this repo

This repo contains some template code for .NET Core applications with security features.

### General Setup

- if running the examples on Linux, make sure you install the dev TLS certificates by running the `setup_linux_dev_tls_certs.sh` script, as the process for trusting the certificates is different on Linux than Windows or MacOS.

### Authentication - JWT (Public Key Encryption)

A basic example of microservices that use JWT authentication. The IdentityService will log in valid users and issue a signed JWT with the RSA256 algorithm. The AuthnService is a simple REST API service that uses JWT Bearer authentication and validates the token against the IdentityService public key.

#### Running the IdentityService

```
cd IdentityService

# create the private/public keys and certificates, and copy them to locations specified in the config JSON files
./create_pfx.sh
cp certificate.pfx /var/tmp/democertificate.pfx
cp public-key.pem /var/tmp/
export CertificatePassword=ReplaceWithYourPassword 

dotnet run
```

#### Running the AuthnService

```
cd AuthnService
dotnet run
```

#### Test the Authentication
```
# log in and obtain a JWT bearer token from the IdentityService
curl -kv -d '{"Username":"sampleUser","Password":"samplePassword"}' -H 'content-type: application/json' https://127.0.0.1:5003/auth/login

# Invoke an AuthnService endpoint, providing the JWT bearer token from the IdentityService
curl -kv -H 'Authorization: Bearer <insert_your_token_here>' 'https://127.0.0.1:5001/Ratings/calculate?likelihood=high&impact=medium'
```
