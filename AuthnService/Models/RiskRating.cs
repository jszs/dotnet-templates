namespace AuthnService.Models;

public class RiskRating
{
    public string? Risk { get; set; }
    public int RiskScore { get; set; } = 0;
    public string? Impact { get; set; }
    public string? Likelihood { get; set; }
}
