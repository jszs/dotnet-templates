using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.OpenApi.Models;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        
        app.UseRouting();
		app.UseAuthentication();
		app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
		
        // Other configuration for the request pipeline can be added here.
    }

    public void ConfigureServices(IServiceCollection services)
    {
		RsaSecurityKey rsaPublicKey = CreateServicesAndLoadRsaPublicKey(services);
		
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
				options.IncludeErrorDetails = true; //disable in production
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _configuration["Jwt:Issuer"],
                    ValidAudience = _configuration["Jwt:Audience"],
                    IssuerSigningKey = rsaPublicKey
                };
				options.Events = new JwtBearerEvents  //disable in production
				{
					OnAuthenticationFailed = context =>
					{
						// Log the detailed error information
						Console.WriteLine($"Authentication failed: {context.Exception}");
						return Task.CompletedTask;
					}
				};
            });

        services.AddControllers();
		
        // Other services can be added here

        // Register the Swagger generator
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "IdentityService API", Version = "v1" });
        });
    }

	private RsaSecurityKey CreateServicesAndLoadRsaPublicKey(IServiceCollection services)
	{
        var publicKeyPath = _configuration["JWTPublicKeyPath"];
		RSA rsa = RSA.Create();
		services.AddSingleton<RSA>(rsa);
        var publicKeyPemString = File.ReadAllText(publicKeyPath);
		rsa.ImportFromPem(publicKeyPemString);
		RsaSecurityKey rsaPublicKey = new RsaSecurityKey(rsa);
		services.AddSingleton<RsaSecurityKey>(rsaPublicKey);
		return rsaPublicKey;
	}

}
