using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AuthnService.Models;

namespace AuthnService.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize]
public class RatingsController : ControllerBase
{
	// convert impact and likelihood level names to integer scores
	private Dictionary<string, int> scoreLookup = new Dictionary<string, int>();
	private Dictionary<int, string> reverseLookup = new Dictionary<int, string>();
		
    private readonly ILogger<RatingsController> _logger;

    public RatingsController(ILogger<RatingsController> logger)
    {
        _logger = logger;

        // Add lookup data to the dictionary
        scoreLookup["very high"] = 4;
        scoreLookup["high"] = 3;
        scoreLookup["medium"] = 2;
        scoreLookup["low"] = 1;

		foreach (var kvp in scoreLookup)
		{
			reverseLookup[kvp.Value] = kvp.Key;
		}
    }

    [HttpGet]
	[Route("ratings")]
    public RatingsValues GetRatings()
	{
		return new RatingsValues
		{
			Values = scoreLookup.Keys.ToArray()
		};
	}
	
    [HttpGet]
	[Route("calculate")]
    public RiskRating GetRatings(string impact, string likelihood)
    {
		int impactScore = GetScore(impact);
		int likelihoodScore = GetScore(likelihood);
		int riskScore = impactScore * likelihoodScore;
		
        return new RiskRating
        {
			Risk = GetRiskName(riskScore),
            RiskScore = riskScore,
			Impact = impact,
			Likelihood = likelihood
        };
    }

	private int GetScore(string levelName)
	{
		return scoreLookup[levelName];
	}	

	private string GetRiskName(int score)
	{
		int tmpScore = (int)(score/4);
		tmpScore = (tmpScore >= 3) ? 4 : tmpScore + 1;
		return reverseLookup[tmpScore];
	}	

}
