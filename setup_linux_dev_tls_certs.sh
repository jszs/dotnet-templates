#!/bin/bash

# as per https://github.com/dotnet/aspnetcore/issues/32842:
# the Linux setup process for .NET TLS development environment certificates is different to Windows/MacOS
# run this script on Linux to generate or convert the dev certificate, and install it so that .NET framework finds it and uses it for HTTPS services

DEV_CERT_PATH="~/.dotnet/corefx/cryptography/x509stores/my"
TMP_CERT=$(mktemp --suffix=.pem)
EXISTING_CERT_NUM=$(ls -1 ${DEV_CERT_PATH}/*.pfx | head -n 1 | cut -d '/' -f 8)

if [ -z "$EXISTING_CERT_NUM" ]
then
	# generate a new dev certificate
	dotnet dev-certs https -ep "$TMP_CERT" --format PEM
else
	echo "Using existing dev certificate ${DEV_CERT_PATH}/${EXISTING_CERT_NUM}"
	openssl pkcs12 -in "${DEV_CERT_PATH}/${EXISTING_CERT_NUM}" -out "$TMP_CERT" -nodes
fi
SHA256NAME=$(sha256sum "$TMP_CERT" | cut -d ' ' -f 1)
mv "$TMP_CERT" /usr/lib/ssl/certs/aspnetcore-localhost-https-${SHA256NAME}.pem
